#!/bin/bash
echo "Starting Shuffle"
TEMPEXT=".new"

for ((i = 0 ; i < 100 ; i++)); do
RAND1=`shuf -i 1-10000 -n1`
RAND2=`shuf -i 1-10000 -n1`
echo "Swapping file $RAND1 with $RAND2"
if [[ "$RAND1" == "$RAND2" ]]; then
    echo "numbers are the same, skipping"
else
    mv $RAND1 $RAND2$TEMPEXT
    mv $RAND2 $RAND1
    mv $RAND2$TEMPEXT $RAND2
    echo "shuffle complete - shuffled $RAND1 with $RAND2"
fi

done