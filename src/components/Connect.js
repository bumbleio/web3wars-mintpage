import React from 'react';
import Button from './Button';
import { useState } from 'react';
import logo from '../artifacts/images/LOGOsmall.png'


const Connect = ({onClickconnect}) => { 

    return (

        <div className='connect'>
            <img src={logo} alt="logo" className="connectlogo"></img>
            <Button color='Black' text='Connect' onClick={onClickconnect} />



    
            
        </div>
    )
}


export default Connect;