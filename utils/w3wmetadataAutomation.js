
const { clear } = require("console");
const fs = require("fs");
var metadataList = [];
var attributesList = [];
var data = [];
var globalCount;
var randomtraitlist = ["Strength", "Stealth", "Intelligence", "Dexterity", "Vitality", "Magic", "Holyness", "Wisdom", "Lawful"];
var clanmember = "Ape Fathers";
var classmember = "Civilian";
var clancount = 30;
//  function reads csv and captures data in 2 array header and data

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}


function specialPower() {
  var power = "";
  var powerint = randomIntFromInterval(1,100);
//return powerint;
if (powerint > 0 && powerint <= 10) {
  var power = "Elemental Control"
  return power;
} else if (powerint > 10 && powerint <= 20)  {
  var power = "X-ray Vision"
  return power;
} else if (powerint > 20 && powerint <= 30)  {
  var power = "Optic Blasts"
  return power;
} else if (powerint > 30 && powerint <= 40)  {
  var power = "Intangibility"
  return power;
} else if (powerint > 40 && powerint <= 50)  {
  var power = "Shape Shifting"
  return power;
} else if (powerint > 50 && powerint <= 60)  {
  var power = "Power Absorption"
  return power;
} else if (powerint > 60 && powerint <= 70)  {
  var power = "Regeneration"
  return power;
} else if (powerint > 70 && powerint <= 80)  {
  var power = "Invisibility "
  return power;
} else if (powerint > 80 && powerint <= 90)  {
  var power = "Force Fields"
  return power;
} else if (powerint > 10 && powerint <= 20)  {
  var power = "Flight"
  return power;
} else if (powerint > 90 && powerint <= 100)  {
  var power = "Teleportation"
  return power;
}

}

function clanLoop(){
 // need to work out where to start
  for(var i = 1; i <= clancount; i++) {
      globalCount = i;
      var filename = i;
      //console.log("var type " + typeof filename)
      

      addAttributes();
      // data.length = 0;
     writeMetaData(JSON.stringify(metadataList, null, 2));
     fs.writeFileSync(filename.toString(), JSON.stringify(metadataList[i -1], null, 2));
  }

  //console.log(metadataList)
  

}


const addAttributes = () => {
  console.log("got there")
  attributesList = [];

  for (let i = 0; i < randomtraitlist.length; i++) {
    attributesList.push({
      trait_type: randomtraitlist[i],
      value: randomIntFromInterval(1, 10),
    });
  }


console.log("atrributes to add " + attributesList);
addMetadata(attributesList)
};


const addMetadata = (attributesList) => {
    console.log("global count " + globalCount)
    let dateTime = Date.now();
    let tempMetadata = {
      description: "Web3wars",
      image: 'ipfs://QmRFZfbsZi6ZAjKFxVeFadqtewwsrjKjTyp1CD557w8uSM/' + globalCount.toString() +'.png',
      clan: clanmember,
      Tier: classmember,
      Power: specialPower(),
      attributes: attributesList,
    };
    console.log("adding to metadataarray")
    metadataList.push(tempMetadata);
  };


  const writeMetaData = (_data) => {
    fs.writeFileSync(`./_metadata.json`, _data);
  };



  clanLoop();

