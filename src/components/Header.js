
import { useState } from 'react';

// import BAFC contract ABI
//import bayc from '../artifacts/abis/bayc.json'



// this is a arrow function compare to normal function
const Header = ({onClickheader}) => {

    const [tokenid, setTokenid] = useState('')
    var numbers = /^[0-9]+$/;

    const onSubmit = (e) => {

        e.preventDefault()
        if(!tokenid || tokenid > 20) {
            alert('Please select number of tokens you wish to mint - Max 20')
            return
        }

        if(tokenid.match(numbers)){
        console.log("got here")
        onClickheader(tokenid)
        setTokenid('')
        } 
        else {
          alert('Numbers can only be used- Max 2')
          return
        }
    }

    return (
    <header className='header'>
        
    <form className='add-form' onSubmit={onSubmit}>
      <div className='form-control'>
        <label>Number of tokens to mint:</label>
        <input
          type='text'
          placeholder='1-20'
          value={tokenid}
          onChange={(e) => setTokenid(e.target.value)}
        />
        <input type='submit' value='MINT FOR 80 MATIC' className='btn btn-block' />
      </div>  
    </form> 
    
      </header>
    )
    }       

export default Header;