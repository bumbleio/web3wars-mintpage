
const { clear } = require("console");
const fs = require("fs");
var metadataList = [];
var attributesList = [];
var data = [];
var startingtokennumber = 2912; // Change for clan
var fileCount = 0
var randomtraitlist = ["Strength", "Stealth", "Intelligence", "Dexterity", "Vitality", "Magic", "Holyness", "Wisdom", "Lawful", "Arrogance", "Beauty", "Ambition"];
var clanmember = "No Coiners"; // Change for clan
var classmember = ["Civilian","Cadet","Ring Leader","Sergeant","Commander","Officer", "Captain", "Lieutenant","Major", "General", "Clan Leader"]
var classmemberCount = 0;
var classsize = [150 ,100 ,25 ,20 ,18 ,15 ,12 ,10 ,8 ,5 ,1] // Change for clan

//  function reads csv and captures data in 2 array header and data

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}


function specialPower() {
  var power = "";
  var powerint = randomIntFromInterval(1,100);
//return powerint;
if (powerint > 0 && powerint <= 1) {
  var power = "Elemental Control"
  return power;
} else if (powerint > 1 && powerint <= 5) {
  var power = "X-ray Vision"
  return power;
} else if (powerint > 5 && powerint <= 15)  {
  var power = "Optic Blast"
  return power;
} else if (powerint > 15 && powerint <= 27)  {
  var power = "Intangibility"
  return power;
} else if (powerint > 27 && powerint <= 33)  {
  var power = "Shape Shifting"
  return power;
} else if (powerint > 33 && powerint <= 47)  {
  var power = "Power Absorption"
  return power;
} else if (powerint > 47 && powerint <= 60)  {
  var power = "Regeneration"
  return power;
} else if (powerint > 60 && powerint <= 70)  {
  var power = "Invisibility "
  return power;
} else if (powerint > 70 && powerint <= 77)  {
  var power = "Force Field"
  return power;
} else if (powerint > 77 && powerint <= 85)  {
  var power = "Flight"
  return power;
} else if (powerint > 85 && powerint <= 87)  {
  var power = "Teleportation"
  return power;
}
else if (powerint > 87 && powerint <= 91)  {
  var power = "Super Strength"
  return power;
}
else if (powerint > 91 && powerint <= 98)  {
  var power = "Super Speed"
  return power;
}
else if (powerint > 98 && powerint <= 100)  {
  var power = "TeleKinesis"
  return power;
}

}



function classLoop(){
  // need to work out where to start
   for(var i = 0; i < classmember.length; i++) {
    console.log("var type " + typeof classmemberCount + " value:" + classmemberCount)
      for(var t = 1; t <= classsize[i]; t++) {
        startingtokennumber = startingtokennumber + 1;
        fileCount = fileCount + 1;
        console.log(startingtokennumber);
        addAttributes(classmember[i]);      
        writeMetaData(JSON.stringify(metadataList, null, 2));
        console.log("made it here")
        fs.writeFileSync('../web3wars-allmetadata/' + startingtokennumber.toString(), JSON.stringify(metadataList[fileCount -1], null, 2));
 
     
      }
      classmemberCount = classmemberCount + 1;
      
   }
 
 }


 const addAttributes = (classtype) => {
  console.log("class member: " + classtype)
  attributesList = [];

  attributesList.push({
    trait_type: "Clan",
    value: clanmember,
  });

  attributesList.push({
    trait_type: "Clan Rank",
    value: classmember[classmemberCount],
  });
  
  attributesList.push({
    trait_type: "Super Power",
    value: specialPower(),
  });


  if (classtype == "Civilian") {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(1, 4),
        max_value: 10,
      });
    }
  } else if (classtype == "Cadet") {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(1, 5),
        max_value: 10,
      });
    }
  } else if (classtype == "Ring Leader")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(1, 6),
        max_value: 10,
      });
    }
  } else if (classtype == "Sergeant")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(2, 6),
        max_value: 10,
      });
    }
  } else if (classtype == "Commander")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(2, 7),
        max_value: 10,
      });
    }
  } else if (classtype == "Officer")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(3, 7),
        max_value: 10,
      });
    }
  } else if (classtype == "Captain")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(4, 8),
        max_value: 10,
      });
    }
  } else if (classtype == "Lieutenant")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(5, 8),
        max_value: 10,
      });
    }
  } else if (classtype == "Major")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(6, 9),
        max_value: 10,
      });
    }
  } else if (classtype == "General")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(7, 9),
        max_value: 10,
      });
    }
  } else if (classtype == "Clan Leader")  {
    console.log("matched " + classtype)
    for (let i = 0; i < randomtraitlist.length; i++) {
      attributesList.push({
        trait_type: randomtraitlist[i],
        value: randomIntFromInterval(7, 10),
        max_value: 10,
      });
    }
  }
    

console.log("atrributes to add " + attributesList);
addMetadata(attributesList)
};
// Change for clan image
const addMetadata = (attributesList) => {
    console.log("global count " + startingtokennumber)
    let dateTime = Date.now();
    let tempMetadata = {
      description: "Web3Wars - A solar surge has forced the human race underground. As a result, the Metaverse is now a foundational component of the global economy. Control of this digital realm is up for grabs. Opposing clans are actively recruiting to secure their dominance. Which clan is your destiny? Visit [web3wars.io](https://web3wars.io/) to learn more.",
      image: 'ipfs://QmZR4iqcURoKJbjFUcf8i3EK5Jfj1Z6wpRC4SmLneNyrp5/9.png', 
      attributes: attributesList,
    };
    console.log("adding to metadataarray")
    metadataList.push(tempMetadata);
    console.log("added to metadataarray")
  };


  const writeMetaData = (_data) => {
    fs.writeFileSync(`./_metadata.json`, _data);
  };



  // clanLoop();
  classLoop();

