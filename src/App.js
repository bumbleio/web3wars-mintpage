import React,{useState , useEffect} from 'react';
import { ethers } from 'ethers'
import w3w from './artifacts/contracts/w3w.sol/w3w.json'

// import components
import Header from './components/Header';
import Footer from './components/Footer';
import Connect from './components/Connect';

//import libraries
var converter = require('hex2dec');
const request = require('request');



function App() {
  //polygon wew contract address
  const bonesAddress = "0xCa29fe9026c635e399d04A4fb386d902474f79a7";

  // in gwei
  const mintFee = ""
  
  // state
  const [totalsupply, setTotalsupply] = useState('');
  const [apeimageurls, setApeimageurls] = useState([]);  
 
  // function to invoke metamask
  async function requestAccount() {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
    console.log("clicked")
  }
  
  async function mint(numberTokens) {
    const totalvalue = numberTokens * 80000000000000000000;
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const activeAddresswindow = await window.ethereum.request({ method: 'eth_requestAccounts' });
      const provider1 = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider1.getSigner()
      const contract1 = new ethers.Contract(bonesAddress, w3w.abi, signer)
      //mintpass is failing  failing -need to fix - coul be deploying wrong check deply.js
      // const transaction1 = await contract1.mint(activeAddresswindow[0],numberTokens,{value: ethers.utils.parseUnits(totalvalue.toString(), 'wei')})
      try {
      const transaction1 = await contract1.mint(numberTokens,{value: ethers.utils.parseUnits(totalvalue.toString(), 'wei')})
      await transaction1.wait()
      console.log("Mint Successful calling total supply,wait 30 seconds for update ")
      await sleep(30000)
      totalSupply();
      //const data = await contract1.totalSupply();
      //var numberOfPassesOwned = converter.hexToDec(data._hex);
      //console.log(typeof numberOfPassesOwned);
      //console.log(numberOfPassesOwned)
      //setTotalsupply(numberOfPassesOwned);
      //console.log('data: ', data);
      //console.log(typeof data);
      } catch (err) {
        console.log("Error is: ", err);
        alert("Make sure your connected to Polygon Mainnet and have sufficent funds")
      }
    }
  }
  
  async function totalSupply() {
    console.log("got to totalsupply function")
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(bonesAddress, w3w.abi, provider)
        try {
          const data = await contract.totalSupply();
          var numberOfPassesOwned = converter.hexToDec(data._hex);
          setTotalsupply(numberOfPassesOwned);
          // setContractname(data);
          //console.log('stateis' + contractname);
          //getAllTokenMetaData()
        } catch (err) {
          console.log("Error: ", err);
          alert('Please make sure you are using Metamask and are connected to Polygon Mainnet - https://docs.polygon.technology/docs/develop/metamask/config-polygon-on-metamask/');
        }
      }    
    }

    async function sayHello() {
      alert('Hello!');
      console.log('ddd')
    }

    async function getAllTokenMetaData() {
      //resets state for each connect
      setApeimageurls([]);
      if (typeof window.ethereum !== 'undefined') {
        const ipfsGatewayBase = "https://gateway.pinata.cloud/ipfs/"
        console.log("connecting to wallet")
        await requestAccount();
        // get address of wallet
        // add to variable and console log it const address
        const activeAddresswindow = await window.ethereum.request({ method: 'eth_requestAccounts' });
        console.log(activeAddresswindow[0]);
   
        //with address variable call contract function balaceof to find out how many tokens of the contact it owns add number to variable
        // const numberOfTokensOwned
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        console.log("providerset");
        const contract = new ethers.Contract(bonesAddress, w3w.abi, provider)
        console.log("providerset11");
        const hexOfTokensOwned = await contract.balanceOf(activeAddresswindow[0]);
        var numberOfTokensOwned = converter.hexToDec(hexOfTokensOwned._hex);
        console.log("tokensowned: " + numberOfTokensOwned)
        console.log(typeof numberOfTokensOwned);
        parseInt(numberOfTokensOwned);
        
        for (let i = 0; i < parseInt(numberOfTokensOwned); i++) {
          console.log("The number is " + i);
          const tokenIndexOwned = await contract.tokenOfOwnerByIndex(activeAddresswindow[0],i);      
          const tokenURI = await contract.tokenURI(converter.hexToDec(tokenIndexOwned._hex));
          request(ipfsGatewayBase + tokenURI.substring(7) , { json: true }, (err, res, body) => {
            if (err) { return console.log(err); }
              console.log(ipfsGatewayBase + body.image.substring(7))
              setApeimageurls(apeimageurls => apeimageurls.concat(ipfsGatewayBase + body.image.substring(7)));
            });  
        }
      }
    }


    function sleep(ms) {
      return new Promise((resolve) => {
        setTimeout(resolve, ms);
      });
    }





  return (
    <div className='container' >
      <Connect onClickconnect={totalSupply}/>
      



  
      
      
      <Header onClickheader={mint} />
      

      <Footer supply={totalsupply}/>
      
  
    </div>
  );
}

export default App;
